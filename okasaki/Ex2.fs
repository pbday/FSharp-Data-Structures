﻿module Ex2

//Ex 2.1
let rec suffixes l =
    match l with
    | [] -> [[]]
    | (_::t) -> l::suffixes t
 
open Structures.SimpleSet
//Ex 2.2
let member' x = function
    | E -> false
    | T(_,y,_)as s ->
        let rec aux can = function
            | E -> can = x
            | T (l,y,_) when x < y -> aux x l
            | T (_,y,r)  -> aux can r
        aux y s   

//Rather than checking equality at each point you can carry through a potential
//equals values and just check it at the end
     
//Ex2.3

exception AlreadyInSet

let insert x set = 
    let rec aux = function
        | E -> T(E,x,E)
        | T(l,y,r) when x < y -> T(aux l, y, r) 
        | T(l,y,r) when x > y -> T(l,y,aux r)
        | s -> raise AlreadyInSet
    try
        aux set
    with | AlreadyInSet -> set

//Rather than copy the entire tree when an element is already in the set,
// an exception can be thrown to just return the original

//Ex2.4

let insert2 x set =
    let rec aux can = function
        | E when can = x -> raise AlreadyInSet 
        | E -> T(E,x,E)
        | T(l,y,r) when x < y -> T(aux can l, y, r) 
        | T(l,y,r) -> T(l,y,aux y r)
    try
        aux x set
    with | AlreadyInSet -> set    
// This combine the two above features

//Ex2.5 a)

/// <summary>
/// Creates a complete binary tree of depth d with x stored in every node. Function to run in O(d) time.
/// </summary>
let rec complete x = function
    | 0 -> E
    | n ->  
        let t = complete x (n-1)
        T(t,x,t) 
//Using the same version of something when it is the same as it cant change is more efficient

let rec complete2 x = function
    | 0 -> E
    | n -> 
        let create2 m =
            match m % 2 with
            | 0 -> 
                let t = complete2 x (m/2)
                T(t,x,t)
            | 1 -> 
                T(complete2 x (m/2), x, complete2 x ((m/2) + 1))
            | _ -> failwith "Impossible to match here"
        create2 (n-1)

module FiniteMap=
    type KVP<'K,'V when 'K: comparison> = 
        | E
        | T of KVP<'K,'V> * 'K * 'V * KVP<'K,'V> 

    exception NotFound

    let empty = E

    let rec bind k v = function
    | E -> T(E,k,v,E)
    | T(l,k',v',r) when k < k' -> T(bind k v l,k',v',r)
    | T(l,k',v',r) when k > k' -> T(l,k',v',bind k v r)
    | T(l,k,v',r) -> T(l,k,v,r)  

    let rec lookup k = function
    | E -> raise NotFound
    | T(l,k',_,_) when k < k' -> lookup k l
    | T(_,k',_,r) when k > k' -> lookup k r
    | T(_,k,v,_) -> v

module test=
    let s1 = empty |> insert 5 |> insert 9 |> insert 6 |> insert 4
    let m5 = s1 |> member' 6
    let m10 = s1 |> member' 10 
    let s2 = s1 |> insert 5