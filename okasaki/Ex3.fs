﻿module Ex3

//Ex 3.2

open Heap

let rec insertDirect x = function
    | E -> H(1,x,E,E)
    | H(rk,x',l,r) as h when x < x' -> H(1,x,h,E)
    | H(rk,x',l,r) as h -> MakeT x' l (insertDirect x r) 
        
//Ex 3.3

let divAndCon (f:'a->'a->'a) l =
    let rec aux (hl :'a list) (rl:'a list) = 
        match hl, rl with
        | (h::[]), []       -> h
        | _, []             -> aux [] hl
        | (h::t), (rh::[])  -> aux [(f rh h)] t
        | _, (h1::h2::t)    -> aux ((f h1 h2) :: hl) t
        | _, [h] -> h
    aux [] l

let ofList l =
    divAndCon merge (l |> List.map(fun x -> H(1,x,E,E)))    
    
//Ex 3.4

let size = function
    | E -> 0
    | H(s,_,_,_) -> s

let MakeTW x a b =
    if (size a > size b)
        then H((size a) + (size b) + 1, x, a, b)
        else H((size a) + (size b) + 1, x, b, a)

let empty = E

let rec mergeW h1 h2 =
    match h1, h2 with
    | E, h | h, E -> h
    | H(_,x1,l,r), H(_,x2,_,_) when x1 < x2 -> MakeTW x1 l (mergeW r h2)
    | _, H(_,x,l,r) -> MakeTW x l (mergeW r h1) 

let insertW x h = mergeW h (H(1,x,E,E))

//3.4 c - only top down

let rec mergeWTD h1 h2 =
    match h1, h2 with
    | E, h | h, E -> h
    | H(s1,x1,l,r), H(s2,x2,_,_) when x1 < x2 -> 
        if ((s2 + size r) > size l) 
            then H(s1 + s2, x1,mergeWTD r h2,l)
            else H(s1+s2, x1, l, mergeWTD r h2)

    | H(s1,_,_,_), H(s2,x,l,r) ->
        if ((s1 + size r) > size l)
            then H(s1+s2,x,mergeWTD r h1,l)
            else H(s1+s2,x,l,mergeWTD r h1)

//3.5 - find min directly rather than a call to removeMinTree

exception EmptyTree
open BinomialHeap
let rec findMinDir = function
    | [] -> raise EmptyTree
    | [Node(_,m,_)] -> m
    | Node(_,m,_) :: t -> min m (findMinDir t) 

//3.6 - Implementing a binomial tree withoout the redundant rank information, these are 
// stored at the top of the tree

type TreeNoR<'T when 'T : comparison> = Node of 'T * (TreeNoR<'T> list)
type BinHeapNoR<'T when 'T:comparison> = (int * TreeNoR<'T>) list

let link t1 t2 =
    match t1, t2 with
    | Node(x1,c1), Node(x2,c2) ->
        if (x1<=x2) 
            then Node(x1,t2::c1)
            else Node(x2,t1::c2)

let root = function
    | Node(r,_) -> r

let rec insTree t1 ts =
    match t1, ts with
    | _, [] -> [t1]
    | (r1,t), ((r2,t')::ts') ->
        if r1 < r2 
            then t1 :: ts
            else insTree (r1+1, link t t') ts'

let insert x ts = insTree (0,Node(x,[])) ts

let rec merge ts1 ts2 =
    match ts1, ts2 with
    |  ([], t) | (t, []) -> t
    | ((r1,t1)::ts1'), ((r2,t2)::ts2')->
        if r1 < r2
            then (r1,t1) :: merge ts1' ts2
        elif r2 < r1
            then (r2,t2) :: merge ts1 ts2'
        else
            insTree (r1+1,(link t1 t2)) (merge ts1' ts2')   

let rec removeMinTree = function
    | [t] -> t, []
    | ((r,t)::ts) -> 
        let (r',t'), ts' = removeMinTree ts
        if root t < root t' then ((r,t),ts) else ((r',t'), (r,t)::ts')
    | [] -> raise EmptyTree
        
let findMin ts = ts |> removeMinTree |> fst |> snd |> root 

let deleteMin ts = 
    match removeMinTree ts with
    | (r, Node(_,ts1)), ts2 ->
        merge (ts1 |> List.rev |> List.mapi(fun i x -> (i,x))) ts2   

//3.7 - Functor that stores min to make it retrievable in O(1) time

type ExplicitMinHeap<'T when 'T:comparison> = 
    | E
    | H of 'T * BinHeapNoR<'T>

let insertMH x = function
    | E -> H(x,insert x [])
    | H(m,h) -> H(min m x, insert x h)

let findMinMH = function
    | E -> raise EmptyTree
    | H(m,_) -> m

let mergeMH h1 h2 =
    match h1, h2 with
    | E, E -> E
    | E, h | h, E -> h
    | H(m1,h1'), H(m2,h2') -> H(min m1 m2, merge h1' h2')

let deleteMinMH = function
    | E -> raise EmptyTree
    | H(m,h) -> 
        let h' = deleteMin h
        H(findMin h',h')

//TODO: Ex 3.10

let treeHeight n =
    let rec aux h c = if (c * 2 <= n) then aux (h+1) (c * 2) else h
    aux 0 1  

module ex3_9=
    open RBSet
    let fromOrdList lst =
        let rec Aux (l:int list) n =
            if (n=0) then E,l
            elif (n=1) 
                then T(R,E,l.Head,E), l.Tail
                else
                    let ln = n / 2 
                    let rn = n - ln - 1
                    let lT, rem = Aux l ln
                    let rT, rtn = Aux rem.Tail rn
                    let BHead = function
                        | E -> true
                        | T(B,_,_,_) -> true
                        | _ -> false
                    let colour = if (BHead lT && BHead rT) then R else B
                    (T(colour,lT,rem.Head,rT), rtn) 
        Aux lst lst.Length |> fst

    //Good example of divide and conquering on a singly inked list in O(n) time
    
module ex3_10=
    open RBSet
    //RB Set can be improved by considering left and right balance separately
    let lBalance = function
        | (B, T(R,T(R,a,x,b), y, c),z,d)
        | (B, T(R,a,x,T(R,b,y,c)),z,d) -> T(R,T(B,a,x,b),y,T(B,c,z,d))
        | body -> T body

    let rBalance = function
        | (B, a,x,T(R,T(R,b,y,c),z,d))
        | (B,a,x,T(R,b,y,T(R,c,z,d))) -> T(R,T(B,a,x,b),y,T(B,c,z,d))
        | body -> T body

    let insert x s= 
        let rec ins = function
            | E -> T(R,E,x,E)
            | T(colour,a,y,b) as s' ->
                if (x < y) then lBalance (colour,ins a,y,b)
                elif (x > y) then rBalance (colour, a, y, ins b)
                else s'
        match ins s with
        | T(_,a,y,b) -> T(B,a,y,b)
        | E -> failwith "guarunteed to be non empty"

    //TODO: remove for grandchildren too!.

    let add x y= 
        lazy
            printfn "Calculating"
            x + y

    (add 1 2).Force