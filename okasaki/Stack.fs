﻿module Structures

type CustomStack<'T> =
    | Nil
    | Con of 'T * 'T CustomStack
    member t.cons i =
        Con(i,t)
    static member Empty() : CustomStack<'T> = Nil
    static member (++) (s1: CustomStack<'T>, s2: CustomStack<'T>) = 
        match s1, s2 with
        | Nil, _ -> s2
        | _, Nil -> s1
        | Con(h,t), _ -> Con(h,t ++ s2)        
    member t.isEmpty = 
        match t with 
        | Nil -> true
        | Con _ -> false
    member t.head = 
        match t with
        | Nil -> failwith "Stack is empty"
        | Con(h,_) -> h
    member t.tail =
        match t with
        | Nil -> failwith "Stack is empty"
        | Con(_,tail) -> tail  

        

    module CustomStack = 
        let empty<'T> : CustomStack<'T> = CustomStack.Empty() 
        let cons i (s:CustomStack<_>) = s.cons i
        let rec update v i (s:CustomStack<_>) =
            match i, s with
            | 0, Con(h,t) -> Con(v,t)
            | _, Nil -> failwith "Index out of range"
            | n, Con(h,t) -> cons h (update v (n-1) t) 

let s0 = CustomStack.empty |> CustomStack.cons 2
let s1 = s0.cons 2
let s2 = s1.tail
let s3 = s1.cons 3
let s4 = s3.tail
let s5 = CustomStack.update 30 1 s3


let str0 = CustomStack.empty |> CustomStack.cons "Hello"
let str1 = str0.cons "World"
let str2 = str1 |> CustomStack.update "Goodbye" 1