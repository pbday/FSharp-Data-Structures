﻿module TreeSet

    type Tree<'T when 'T: comparison> =
        | E
        | T of Tree<'T> * 'T * Tree<'T>
    
    let empty = E

    let rec member' x = function 
        | E -> false
        | T(l,y,r) -> 
            if (x < y) then member' x l
            elif (x > y) then member' x r
            else true
            
    let rec insert x = function
        | E -> T(E,x,E)
        | T(l,y,r) when x < y -> T(insert x l, y, r) 
        | T(l,y,r) when x > y -> T(l,y,insert x r)
        | s -> s
          
    module test = 
        let s1 = empty |> insert 5 |> insert 9 |> insert 6 |> insert 4
        let m5 = s1 |> member' 6
        let m10 = s1 |> member' 10