﻿module Stream


type StreamCell<'T> =
    | Nil
    | Cons of 'T * Stream<'T>
and Stream<'T> = Lazy<StreamCell<'T>>

let inline f (x:Lazy<_>) = x.Force()

let rec append (s1:Stream<_>) (s2:Stream<_>)  = 
    match f s1 with
    | Nil -> s2
    | Cons(x,s) -> 
        printfn "Append value calculated"
        lazy (Cons(x, append s s2))

let empty() = Lazy.CreateFromValue Nil

let insert x s= Lazy.CreateFromValue (Cons(x,s))

let head s = 
    match f s with
    | Nil -> failwith "Empty list"
    | Cons(x,_) -> x

let tail s =
    match f s with
    | Nil -> failwith "Empty list"
    | Cons(_,s') -> s'


let l1 = empty() |> insert 3 |> insert 2 |> insert 1
let l2 = empty() |> insert 6 |> insert 5 |> insert 4
let both = append l1 l2

let h1 = head both
let b2 = tail both
let h2 = head b2 
let b3 = tail b2
let h3 = head b3
let b4 = tail b3
let h4 = head b4
let b5 = tail b4
let h5 = head b5

